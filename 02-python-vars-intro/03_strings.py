#!/usr/bin/env python3

nombre="Urko"
print("Hola "+nombre+",¿Te gustaria aprender algo hoy?")


print(nombre.lower())
print(nombre.upper())
print(nombre.capitalize())



print("Richard Feynman: \"Hay que tener la mente abierta para que entren nuevas ideas, pero no tan abierta para que se te caiga el cerebro\"")

persona_famosa="Richard Feynman"
mensaje=persona_famosa+": \"Hay que tener la mente abierta para que entren nuevas ideas, pero no tan abierta para que se te caiga el cerebro\""
print(mensaje)

nombre="\tUrko\n"

print(nombre)

print(nombre.lstrip())

print(nombre.rstrip())

print(nombre.strip())