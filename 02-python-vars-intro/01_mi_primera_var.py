#!/usr/bin/env python3

mensaje="Hola mundo"
print(mensaje)

myvar=21
print(myvar)
print(type(myvar))
myvar="string"
print(myvar)
print(type(myvar))

x=y=z=12
a,b,c='urko', 12, 23.4
print(x,y,z)
print(a,b,c)

complejo=2+3j
complejo2=1j
complejo3=complejo*complejo2
print(complejo3)
print(isinstance(complejo3, complex))

cadena = "hola"
print(len(cadena))
print(cadena[0])
print(cadena[-1])
print(cadena[2:4])