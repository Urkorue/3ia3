#!/usr/bin/env python3

#Ejer1
Nombres=["Xabi", "Jon", "Iker"]
Frase="Estas invitado, "
for nombre in Nombres:
    print(Frase+nombre)

#Ejer2
no_asiste="Jon"
nuevo_invitado="Peyo"
Nombres.remove("Jon")
print(no_asiste+" no puede asistir")

Nombres.insert(1, nuevo_invitado)
for nombre in Nombres:
    print(Frase+nombre)

#Ejer3
Nombres.insert(0, "Eneko")
Nombres.insert(2, "Gorka")
Nombres.append("Txema")
for nombre in Nombres:
    print(Frase+nombre)

#Ejer4
Frase2="Ya no estas invitado, "
while len(Nombres)>2:
    print(Frase2+Nombres.pop())

for nombre in Nombres:
    print(Frase+nombre)

del Nombres[0:2]
print(Nombres)

lista=['1', '1', '2']
lista.remove('1')
print(lista)

coches=[2, 4, 3, 2.7, True, False, 1, -1, 0]
coches.sort(reverse=True)
print(coches)
coches.reverse()
print(coches)



