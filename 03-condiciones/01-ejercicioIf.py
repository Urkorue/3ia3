#!/usr/bin/env python3
nombres=['admin','Urko', 'Peru', 'Xabi', 'Ane']

for nombre in nombres:
    if 'admin'==nombre:
        print('Saludos Administrador')
    else:
        print('Bienvenido '+nombre)


nombres.clear()  
if nombres:
    for nombre in nombres:
        if 'admin'==nombre:
            print('Saludos Administrador')
        else:
            print('Bienvenido '+nombre)
else:
    print('Esta vacia')

nombres=['admin','Urko', 'Peru', 'Xabi', 'Ane']
nombres_nuevos=['urko','Nahia', 'xABI', 'Erla']

for nombre_nuevo in nombres_nuevos:
    existe=False
    for nombre in nombres:
        if nombre.upper()==nombre_nuevo.upper():
            existe=True

    if existe:
        print('El nombre '+nombre_nuevo+' ya existe')
    else:
        print('El nombre '+nombre_nuevo+' está disponible')