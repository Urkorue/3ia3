#!/usr/bin/env python3

#Ejer1

from random import randint


file=open('text.txt')

#print(file.read())

#Ejer2

def file_read_from_head(nombre,lineas):
    file=open(nombre)
    for i in range(lineas):
        print(file.readline())

#file_read_from_head('text.txt', 2)

#Ejer3

def doslineas(nombre):
    file=open(nombre, 'a')
    file.write('\nanteultima linea')
    file.write('\nultima linea')

#doslineas('text.txt')

#Ejer4

def file_read_from_tail(nombre, lineas):
    lista=open(nombre).readlines()
    for i in range(len(lista)-1, len(lista)-lineas-1,-1):
        print(lista[i])

#file_read_from_tail('text.txt', 2)

#Ejer5

def file_read(nombre):
    lista=open(nombre).readlines()
    return lista

#print(file_read('text.txt'))

#Ejer6

def file_read2(nombre):
    lista=open(nombre).readlines()
    cadena=''
    for i in range(len(lista)):
        cadena+=lista[i]
    return cadena

#print(file_read2('text.txt'))

#Ejer7

def longest_word(nombre):
    cadena=file_read2(nombre)
    lista_palabras=cadena.split(' ')
    tamanio=0
    resultado=''
    for palabra in lista_palabras:
        if len(palabra)>tamanio:
            resultado=palabra
            tamanio=len(palabra)
    return resultado

#print(longest_word('text.txt'))

#Ejer8

def file_length(nombre):
    lista=open(nombre).readlines()
    return(len(lista))

#print(file_length('text.txt'))

#Ejer9

def palabras(nombre, palabra):
    cadena=file_read2(nombre)
    lista_palabras=cadena.split()
    veces=0
    for palabra2 in lista_palabras:
        if palabra2==palabra:
            veces+=1
    return veces

#print(palabras('text.txt', 'ola'))

#Ejer10

def escribir_lista(nombre, lista):
    file=open(nombre, 'w')
    for i in lista:
        file.write(i+' ')


list=['uno','dos','tres']
#escribir_lista('text.txt', list)

#Ejer11

def copiar_fichero(nombre1, nombre2):
    contenido=open(nombre1).read()
    open(nombre2,'w').write(contenido)

#copiar_fichero('text.txt', 'text2.txt')

#Ejer12

def file_read_random(nombre):
    lista=open(nombre).readlines()
    return lista[randint(0, len(lista))]

#print(file_read_random('text.txt'))

#Ejer13

def cerrado(fichero):
    return fichero.closed

fichero=open('text.txt')
fichero.close()
print(cerrado(fichero))