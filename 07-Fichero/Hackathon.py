#!/usr/bin/env python3
import random

#recibe una matriz de 3x3 y dibuja un tablero
def pintar(tablero):
    print("   A  B  C  ")
    print("1| "+tablero[0][0]+"  "+tablero[1][0]+"  "+tablero[2][0] +" |")
    print("2| "+tablero[0][1]+"  "+tablero[1][1]+"  "+tablero[2][1] +" |")
    print("3| "+tablero[0][2]+"  "+tablero[1][2]+"  "+tablero[2][2] +" |")

#comprueba si hay un ganador, solo comprueba las posibilidades de ganar del ultimo movimiento
#y de las diagonales para hacerlo más eficiente
def comprobarGanador(tablero, x, y):
    if tablero[x][0]==tablero[x][1] and tablero[x][0]==tablero[x][2]:
        return True
    if tablero[0][y]==tablero[1][y] and tablero[0][y]==tablero[2][y]:
        return True
    if tablero[1][1]!=' ' and tablero[0][0]==tablero[1][1] and tablero[0][0]==tablero[2][2]:
        return True
    if tablero[1][1]!=' ' and tablero[0][2]==tablero[1][1] and tablero[2][0]==tablero[1][1]:
        return True
    return False

#Comprueba si hay alguna casilla libre, si no es asi será un empate
def comprobarEmpate(tablero):
    for i in tablero:
        for j in i:
            if j==' ':
                return False
    return True
jugarDeNuevo='S'
victoriasX=0
victoriasO=0
#bucle para jugar varias partidas
while jugarDeNuevo.upper()=='S':
    #cada partida nueva se vacia el tablero
    tablero=[[" "," "," "],[" "," "," "],[" "," "," "]]

    pintar(tablero)

    if random.randint(0,1) ==0:
        jugador='O'
    else:
        jugador='X'
    print('Empieza el jugador '+jugador)
    turnos=0
    #bucle de turnos
    while(True):

        posicion=input("Jugador "+jugador+", introduzca una coordeneda(Ej: 1A): ")
        valido=True
        #validación del input
        if len(posicion)!=2:
            valido=False
        else:
            if posicion[0]!='1' and posicion[0]!='2' and posicion[0]!='3':
                valido=False
            if posicion[1].upper()!='A' and posicion[1].upper()!='B' and posicion[1].upper()!='C':
                valido=False
        if valido:
            y=int(posicion[0])-1
            if posicion[1].upper()=='A':
                x=0
            if posicion[1].upper()=='B':
                x=1
            if posicion[1].upper()=='C':
                x=2
            #comprueba si la casilla está libre, sino avisa al jugador
            if tablero[x][y]==' ':
                tablero[x][y]=jugador 
                turnos+=1
                pintar(tablero)
                if comprobarGanador(tablero, x, y):
                    print('¡El jugador '+jugador+' ha ganado la partida!')
                    print('Enhorabuena')
                    if jugador=='O':
                        victoriasO+=1
                    else:
                        victoriasX+=1
                    break
                if comprobarEmpate(tablero):
                    print('¡Ha habido un empate!')
                    print('Enhorabuena a los dos')

                    break
                if jugador=='O':
                    jugador='X'
                else:
                    jugador='O'
            else:
                print('Esta posición está ocupada.')

        else:
            print('La coordenada no es valida, deve tener un numero del 1 al 3 y una letra de la A a la C.')

    print('La partida ha durado '+str(turnos)+' turnos.')
    print('El jugador X tiene '+str(victoriasX)+' victorias.')
    print('El jugador O tiene '+str(victoriasO)+' victorias.')
    jugarDeNuevo=input("Desea jugar de nuevo? (S/N)")



    

