#!/usr/bin/env python3

def romanToInt(s: str) -> int:
    numeros={'I':1,'V':5,'X':10,'L':50,'C':100,'D':500,'M':1000}
    suma=0
    valor=0
    if len(s)>=1 and len(s)<=15:
        for i in s:
            if numeros[i]>valor:
                valor=numeros[i]-valor

            else:
                suma+=valor
                valor= numeros[i]
        suma+=valor
        return suma
    else:
        return 0

print(romanToInt('MCMXCIV'))#1994
print(romanToInt('MDCCLXXXIII'))#1783
print(romanToInt('MMCMXXXVII'))#2937