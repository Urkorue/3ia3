#!/usr/bin/env python3

n = 2
b = [9, 4]
r = [1, 5]

def redball(n,b,r):
    probR=r[0]/(b[0]+r[0])
    probB=b[0]/(b[0]+r[0])
    for i in range(1,n):
        probR2=((r[1]+1)/(r[1]+b[1]+1))*probR+((r[1])/(r[1]+b[1]+1))*probB
        probB2=((b[1]+1)/(r[1]+b[1]+1))*probB+((b[1])/(r[1]+b[1]+1))*probR
        probR=probR2
        probB=probB2
    return probR

print(redball(n,b,r))