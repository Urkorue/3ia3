#!/usr/bin/env python3


import json

#Ejer1

x =  '{ "name":"John", "age":30, "city":"New York"}'
y = json.loads(x)

print(type(y))
print(y)


#Ejer2

x = {
  "name": "John",
  "age": 30,
  "city": "New York"
}
y = json.dumps(x)
print(y) 


#Ejer3

x = {
  "name": "John",
  "age": 30,
  "married": True,
  "divorced": False,
  "children": ("Ann","Billy"),
  "pets": None,
  "cars": [
    {"model": "BMW 230", "mpg": 27.5},
    {"model": "Ford Edge", "mpg": 24.1}
  ]
}
y=json.dumps(x)

print(y)


#Ejer4

x={'4': 5, '6': 7, '1': 3, '2': 4}
print(json.dumps(x, indent=4, sort_keys=True))