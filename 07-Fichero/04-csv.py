#!/usr/bin/env python3

#Ejer1

nombre_archivo = "csv.csv"
with open(nombre_archivo, "r") as archivo:
    for linea in archivo:
        linea = linea.rstrip()
        print(linea)


        
#Ejer2

nombre_archivo = "csv2.csv"
with open(nombre_archivo, "r") as archivo:

    for linea in archivo:        

        linea = linea.rstrip()
        lista = linea.split("\t")
        print(lista)

#Ejer3

nombre_archivo = "csv.csv"
with open(nombre_archivo, "r") as archivo:
    listaDeListas=[]
    for linea in archivo:        

        linea = linea.rstrip()
        lista = linea.split(",")
        listaDeListas.append(lista)

print(listaDeListas)


#Ejer4
import csv

with open('csv.csv') as f:
    reader = csv.DictReader(f)
    for row in reader:  
         print(row)


#Ejer5

nombre_archivo = "csv.csv"
columna=1
with open(nombre_archivo, "r") as archivo:
    for linea in archivo:
        linea = linea.rstrip()
        lista = linea.split(",")
        columnas=lista[columna]
        print(columnas)

#Ejer6

nombre_archivo = "csv.csv"
columna=1
with open(nombre_archivo, "r") as archivo:
    cabezera=next(archivo, None)
    filas=0
    for linea in archivo:        
        filas+=1
        linea = linea.rstrip()
        print(linea)
    print("Filas: "+str(filas))
    print(cabezera)

