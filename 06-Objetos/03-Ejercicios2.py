#!/usr/bin/env python3

class Persona():
    def __init__(self, nombre='', edad=0, dni=''):
        self.nombre=nombre
        self.edad=edad
        self.dni=dni

     # Getter function
    @property
    def nombre(self):
        return self._nombre

    # Setter function
    @nombre.setter
    def nombre(self, value):
        if not isinstance(value, str):
            print('Error, expected a string')
        self._nombre = value

    # Getter function
    @property
    def edad(self):
        return self._edad

    # Setter function
    @edad.setter
    def edad(self, value):
        if not isinstance(value, int):
            print('Error, expected a int')
        self._edad = value

    
     # Getter function
    @property
    def dni(self):
        return self._dni

    # Setter function
    @dni.setter
    def dni(self, value):

        if not isinstance(value, str) or len(value)!=9:
            print('Error, expected a string with lenght 9')
        self._dni = value

    def mostrar(self):
        print(str(self.nombre)+' '+str(self.edad)+' '+str(self.dni))
    def esMayorDeEdad(self):
        if self.edad>=18:
            return True
        return False

persona=Persona('urko', 25, '12345678A')
persona.mostrar()
print(persona.esMayorDeEdad())


class Cuenta():
    def __init__(self, titular, cantidad=0):
        self.titular=titular
        self.cantidad=cantidad

     # Getter function
    @property
    def titular(self):
        return self._titular

    # Setter function
    @titular.setter
    def titular(self, value):
        self._titular = value

    def mostrar(self):
        print(str(self.titular.nombre)+" "+str(self.cantidad))

    def ingresar(self, cantidad):
        self.cantidad+=cantidad
    def retirar(self, cantidad):
        self.cantidad-=cantidad

cuenta=Cuenta(persona, 10000)
cuenta.mostrar()
cuenta.ingresar(2000)
cuenta.mostrar()

class CuentaJoven(Cuenta):
    def __init__(self, titular, bono, cantidad=0):
        super().__init__(titular, cantidad)
        self._bono=bono
    
     # Getter function
    @property
    def bono(self):
        return self._bono

    # Setter function
    @bono.setter
    def bono(self, value):
        self._bono = value

    def esTitularValido(self):
        if self.titular.edad>=18 and  self.titular.edad<=25:
            return True
        return False
    
    def ingresar(self, cantidad):
        if self.esTitularValido():
            super().ingresar(cantidad)
    def retirar(self, cantidad):
        if self.esTitularValido():
            super().retirar(cantidad)
    def mostrar(self):
        super().mostrar()
        print('Cuenta Joven '+str(self.bono))

cuenta2=CuentaJoven(persona, 1000, 5000)
cuenta2.mostrar()
cuenta2.retirar(500)
cuenta2.mostrar()