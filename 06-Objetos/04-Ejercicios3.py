#!/usr/bin/env python3

class Vehiculo():
    def __init__(self, color, ruedas):
        self.color=color
        self.ruedas=ruedas

     # Getter function
    @property
    def color(self):
        return self._color

    # Setter function
    @color.setter
    def color(self, value):
        self._color = value

         # Getter function
    @property
    def ruedas(self):
        return self._ruedas

    # Setter function
    @ruedas.setter
    def ruedas(self, value):
        self._ruedas = value

class Coche(Vehiculo):
    def __init__(self, color, ruedas, velocidad, cilindrada):
        super().__init__(color, ruedas)
        self.velocidad=velocidad
        self.cilindrada=cilindrada

     # Getter function
    @property
    def velocidad(self):
        return self._velocidad

    # Setter function
    @velocidad.setter
    def velocidad(self, value):
        self._velocidad = value

         # Getter function
    @property
    def cilindrada(self):
        return self._cilindrada

    # Setter function
    @cilindrada.setter
    def cilindrada(self, value):
        self._cilindrada = value

class Camioneta(Coche):
    def __init__(self, color, ruedas, velocidad, cilindrada, carga):
        super().__init__(color, ruedas, velocidad, cilindrada)
        self.carga=carga

     # Getter function
    @property
    def carga(self):
        return self._carga

    # Setter function
    @carga.setter
    def carga(self, value):
        self._carga = value

class Bicicleta(Vehiculo):
    def __init__(self, color, ruedas, tipo):
        super().__init__(color, ruedas)
        self.tipo=tipo

     # Getter function
    @property
    def tipo(self):
        return self._tipo

    # Setter function
    @tipo.setter
    def tipo(self, value):
        self._tipo = value

class Motocicleta(Bicicleta):
    def __init__(self, color, ruedas, tipo, velocidad, cilindrada):
        super().__init__(color, ruedas, tipo)
        self.velocidad=velocidad
        self.cilindrada=cilindrada

     # Getter function
    @property
    def velocidad(self):
        return self._velocidad

    # Setter function
    @velocidad.setter
    def velocidad(self, value):
        self._velocidad = value
    
         # Getter function
    @property
    def cilindrada(self):
        return self._cilindrada

    # Setter function
    @cilindrada.setter
    def cilindrada(self, value):
        self._cilindrada = value

coche=Coche('azul', 4, 200, 300)
camioneta=Camioneta('blanca', 4, 150, 300, 400)
bicicleta=Bicicleta('roja', 2,'monte')
motocicleta=Motocicleta('verde', 2, 'urbana', 120, 100)

lista=[coche,camioneta,bicicleta,motocicleta]

def catalogar(lista):
    for veh in lista:
        print(str(type(veh))+' '+str(vars(veh)))

catalogar(lista)

def catalogar(lista, ruedas):
    cuenta=0
    for veh in lista:
        if veh.ruedas==ruedas:
            cuenta+=1
            print(str(type(veh))+' '+str(vars(veh)))
    print('Se han encontrado '+str(cuenta)+' vehiculos con '+str(ruedas)+' ruedas')    

catalogar(lista, 2)

class Adder():
    def add(self, x, y):
        print('Not implemented!')

class ListAdder(Adder):
    def add(self, x, y):
        return print((str(x)+str(y)))

class DictAdder(Adder):
    def add(self, x, y):
        return print({x,y})


ader=Adder()
listader=ListAdder()
dictader=DictAdder()

ader.add('x','y')
listader.add('x','y')
dictader.add('x','y')