#!/usr/bin/env python3

class Motocicleta():
    nuevo=True
    motor=False
    def __init__(self, color,matricula,combustible_litros, numero_ruedas, marca, modelo, fecha_fabricacion, velocidad_punta, peso):
        self.color=color
        self.matricula=matricula
        self.combustible_litros=combustible_litros
        self.numero_ruedas=numero_ruedas
        self.marca=marca
        self.modelo=modelo
        self.fecha_fabricacion=fecha_fabricacion
        self.velocidad_punta=velocidad_punta
        self.peso=peso

    @property
    def color(self):
        return self._color

    @color.setter
    def color(self, value):
        self._color = value
    
    @property
    def matricula(self):
        return self._matricula

    @matricula.setter
    def matricula(self, value):
        self._matricula = value
    
    @property
    def combustible_litros(self):
        return self._combustible_litros

    @combustible_litros.setter
    def combustible_litros(self, value):
        self._combustible_litros = value

    @property
    def numero_ruedas(self):
        return self._numero_ruedas

    @numero_ruedas.setter
    def numero_ruedas(self, value):
        self._numero_ruedas = value

    @property
    def marca(self):
        return self._marca

    @marca.setter
    def marca(self, value):
        self._marca = value


    @property
    def modelo(self):
        return self._modelo

    @modelo.setter
    def modelo(self, value):
        self._modelo = value


    @property
    def fecha_fabricacion(self):
        return self._fecha_fabricacion

    @fecha_fabricacion.setter
    def fecha_fabricacion(self, value):
        self._fecha_fabricacion = value


    @property
    def velocidad_punta(self):
        return self._velocidad_punta

    @velocidad_punta.setter
    def velocidad_punta(self, value):
        self._velocidad_punta = value


    @property
    def peso(self):
        return self._peso

    @peso.setter
    def peso(self, value):
        self._peso = value

    def arrancar(self):
        if self.motor:
            print('Taba arrankau')
        else:
            self.motor=True
            print('Sa arrankau')

    def detener(self):
        if self.motor:
            self.motor=False
            print('Sa parau')
        else:
            
            print('Ya taba kieto')
    
motocicleta=Motocicleta('azul', '1234ABC', 10, 2, 'Toyota', 'M', 'Hoy', 200, 160)
motocicleta2=Motocicleta(matricula='5678XYZ',color='verde',combustible_litros= 0, numero_ruedas=2, marca='Yamaha', modelo='J',fecha_fabricacion= 'Ayer',velocidad_punta= 250,peso= 150)

motocicleta.arrancar()
motocicleta.arrancar()
motocicleta.detener()
motocicleta.detener()