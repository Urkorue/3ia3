#!/usr/bin/env python3

class SuperClase:
    def __init__(self,x,y):
        print('Soy el constructor SuperClase')
        self.x = x
        self.y = y
    def method_a():
        print('i am method a')

class SubClase(SuperClase):
    def __init__(self,x,y):
        print('Soy el constructor SubClase')
        super().__init__(x,y)

subclase = SubClase(1,2)
#print(subclase.x)
