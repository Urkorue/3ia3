#!/usr/bin/env python3

class Carta:
    listaDePalos = ["Tréboles", "Diamantes", "Corazones","Picas"]
    listaDeValores = ["nada", "As", "2", "3", "4", "5", "6", "7", "8", "9", "10", "Sota", "Reina", "Rey"]
    def __init__(self, palo=0, valor=0):
        self.palo = palo
        self.valor = valor
    def __str__(self):
        return (self.listaDeValores[self.valor] + " de " +
                    self.listaDePalos[self.palo] )
    def __lt__(self, carta2):
        if self.valor<carta2.valor:
            return True
        return False
    def __le__(self, carta2):
        if self.valor<=carta2.valor:
            return True
        return False
    def __eq__(self, carta2):
        if self.valor==carta2.valor:
            return True
        return False
    def __ne__(self, carta2):
        if self.valor!=carta2.valor:
            return True
        return False
    def __gt__(self, carta2):
        if self.valor>carta2.valor:
            return True
        return False
    def __ge__(self, carta2):
        if self.valor>=carta2.valor:
            return True
        return False
tresDeTreboles = Carta(0, 3)        
carta1 = Carta(1, 11)
#print(carta1) # Sota de Diamantes
#print(tresDeTreboles) # 3 de Tréboles
print(tresDeTreboles>carta1)

class Mazo:
    cartas = []
    def __init__(self):
        for palo in range(4):
            for valor in range(1, 14):
                self.cartas.append(Carta(palo, valor))
    def __str__(self):
        s = ""
        for i in range(len(self.cartas)):
            s = s + " "*i + str(self.cartas[i]) + "\n"
        return s  
    def mezclar(self):
        import random
        nCartas = len(self.cartas)
        for i in range(nCartas):
            j = random.randrange(i, nCartas)
            self.cartas[i], self.cartas[j] = self.cartas[j], self.cartas[i]
    def eliminaCarta(self, carta):
        if carta in self.cartas:
            self.cartas.remove(carta)
            return 1
        else:
            return 0  
    def dar_carta(self):
        return self.cartas.pop()

    def esta_vacio(self):
        if self.cartas:
            return False
        return True

mazo=Mazo()
mazo.mezclar()
while not mazo.esta_vacio():
    print(mazo.dar_carta())

print(mazo.esta_vacio())
