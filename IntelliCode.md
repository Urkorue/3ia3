## IntelliCode


### Resumen:

Asistente inteligente que ayuda a programar gracias al autorrellenado. Funciona mediante una IA.


### URL:

[IntelliCode](https://visualstudio.microsoft.com/es/services/intellicode/)


### Instalación:

1. Instale la extension desde la ventana de extensiones de python

2. Asegurese de que está usando Pylance (File -> Preferences -> Settings)

3. Empieze a programar en Python


### Caracteristicas:

Usa el contexto de código actual y patrones del desarrollador para proporcionar una lista dinamica de sugerencias. Se basa en miles de proyectos de código abierto de alta calidad en GitHub. Los resultados forman un modelo que predice las llamadas API más probables y más relevantes.


### Uso:

Puede seleccionar alguna de las sugerencias que genera mientras escribe para ahorrar tiempo, tambien puede seleccionar la accion 'Ignorar sugerencias como esta' si alguna de ellas no se ajusta con sus preferencias. IntelliCode almacena datos de la sesión, por lo que irá ajustando las sugerencias al usuario.


### Valoración:

Me parece una herramienta util, ya que ahorra tiempo al escribir, además de versatil, gracias a que aprende del usuario para mejorar su funcionamiento.