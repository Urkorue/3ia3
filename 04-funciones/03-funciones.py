#!/usr/bin/env python3

def make_album(artista, titulo, pistas=''):
    if pistas:
        pistas_str=str(pistas)
        diccionario={artista:titulo,'pistas':pistas_str}
    else:
        diccionario={artista:titulo}
    print(diccionario)

make_album('S.A.', 'Cuando nada vale nada')
make_album('Iron Maiden', 'Powerslave', 5)


magos=['Txan magoa', 'Merlin', 'Joudini']

def show_magicians(magos):
    for mago in magos:
        print(mago)

def make_great(magos):
    magos2=[]
    for mago in magos:
        magos2.append('El gran mago '+mago)
    return magos2


grandes_magos=make_great(magos)
show_magicians(grandes_magos)
show_magicians(magos)
