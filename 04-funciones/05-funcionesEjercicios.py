#!/usr/bin/env python3

#Ejer 1
numero1=3
numero2=4
numero3=2
def maximo(numero1, numero2, numero3):
    return(max(numero1,numero2,numero3))

print(maximo(numero1,numero2,numero3))
#Ejer2
numeros=[3,5,2,7]
def sumatorio(numeros):
    resultado=0
    for i in numeros:
        resultado+=i
    return(resultado)
print(sumatorio(numeros))

#Ejer3
numeros=[3,5,2,7]
def multiplicatorio(numeros):
    resultado=1
    for i in numeros:
        resultado=resultado*i
    return(resultado)
print(multiplicatorio(numeros))


#Ejer4
def cadenaInversa(cadena):
    return(cadena[::-1])

print(cadenaInversa('olaquease'))

#Ejer5
def factorial(num):
    resultado=1
    for i in range(1,num+1):
        resultado=resultado*i
    return(resultado)

print(factorial(6))

#Ejer6
def dentroFuera(num1,num2,num3):
    if num2>num3:
        if num1<=num2 and num1>=num3:
            return True
        else:
            return False
    else:
        if num1>=num2 and num1<=num3:
            return True
        else:
            return False

print(dentroFuera(4,3,5))

#Ejer7
def mayusMinus(cadena):
    mayuses=0
    minuses=0
    for i in cadena:
        if ord(i)>=65 and ord(i)<=90:
            mayuses+=1
        elif ord(i)>=97 and ord(i)<=122:
            minuses+=1
    print('Mayusculas: '+str(mayuses)+' Minusculas: '+str(minuses))

mayusMinus('AFVedwaADS')

#Ejer8
lstNumeros=[3,3,6,2,7,6]
def noRepe(numeros):
    lista=[]
    for n in numeros:
        if n not in lista:
            lista.append(n)
    return(lista)

print(noRepe(lstNumeros))

#Ejer9
def primo(numero):
    primo=True
    for i in range(2, numero):
        if numero%i==0:
            primo=False
            break
    return(primo)

print(primo(11))

#Ejer10
def soloPares(numeros):
    listaPares=[]
    for i in numeros:
        if i%2==0:
            listaPares.append(i)
    return(listaPares)

print(soloPares(lstNumeros))

#Ejer11
def perfecto(numero):
    lstDivisores=[]
    for i in range(1,numero):
        if numero%i==0:
            lstDivisores.append(i)
    suma=sumatorio(lstDivisores)
    if suma==numero:
        return True
    else:
        return False

print(perfecto(28))

#Ejer12
def palindromo(cadena):
    palin=True
    for i in range(len(cadena)//2):
        if cadena[i].upper()!=cadena[len(cadena)-i-1].upper():
            palin=False
            break
    return(palin)

print(palindromo('abcdefggfedcba'))

#Ejer13

def pascal(numero):
    listaNumViejo=[1]
    for i in range(1, numero+1):
        listaNum=[1]
        cadena=''
        for k in range(i, numero):
            cadena+=' '
        listaNum=[1]
        cadena+=str(listaNum[0])+' '
        for j in range(i-1):
            listaNum.append(listaNumViejo[j]+listaNumViejo[j+1])
            cadena+=str(listaNum[-1])+' '
        print(cadena)
        listaNumViejo=listaNum
        listaNumViejo.append(0)

pascal(5)

#Ejer14

def pangrama(cadena):
    alfabeto=['a','b','c','d','e','f','g','h','i','j','k','l','m','n','ñ','o','p','q','r','s','t','u','v','w','x','y','z']
    for i in cadena:
        if i.lower() in alfabeto:
            alfabeto.remove(i.lower())
    if not alfabeto:
        return True
    else:
        return False


print(pangrama("Un jugoso zumo de piña y kiwi bien frío es exquisito y no lleva alcohol"))


#Ejer15

def ordenar(cadena):
    lista=cadena.split('-')
    lista.sort()
    resultado=''
    for i in lista:
        resultado+=i+'-'
    return(resultado[:-1])

print(ordenar('green-red-yellow-black-white'))


#Ejer16
def listaCuadrados(num):
    lista=[]
    for i in range(1, num+1):
        lista.append(i**2)
    return(lista)

print(listaCuadrados(6))


#Ejer17
def maximum(lista):
    return(max(lista))

print(maximum([3,5,2,8]))

#Ejer18
def buscaNum(lista, numero):
    resul=0
    for n in lista:
        if n==numero:
            resul+=1
    return(resul)

print(buscaNum([4,2,3,6,3,7,3,2,3], 3))

#Ejer19
import math
def circuloArea(radio):
    return(math.pi*(radio**2))

print(circuloArea(5))

#Ejer20

def fibonacci(numero):
    if numero==1:
        return 0
    elif numero==2:
        return 1
    else:
        resultado=fibonacci(numero-1)+fibonacci(numero-2)
        return resultado

print(fibonacci(3))

#Ejer21
def factorial2(num):
    if num==1:
        return 1
    else:
        resultado=factorial(num-1)*num
        return resultado

print(factorial2(4))

#Invent
def euler(num):
    suma=0
    for i in range(num):
        suma+=(1/factorial(i))
    return suma

print(euler(10))